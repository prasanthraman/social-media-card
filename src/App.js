
import './App.css'
import React from 'react'
import Card from './components/Card/Card'

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state =
    {
      cards: [
        {
          id: 1,
          name: "Jane Doe",
          username: "@janedoe",
          title: "Exploring the Great Outdoors",
          description: "Just finished a hike to the summit of Mount Rainier. The views were incredible!",
          image: "https://images.unsplash.com/photo-1551632811-561732d1e306?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8aGlraW5nfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
          hashtags: ["hiking", "nature", "MountRainier"],
          footer: "Hiking and nature photography enthusiast"
        },
        {
          id: 2,
          name: "John Smith",
          username: "@jsmith",
          title: "Cooking up a Storm",
          description: "Trying out a new recipe for shrimp scampi tonight. Fingers crossed it turns out well!",
          image: "https://media.istockphoto.com/id/640305574/photo/cooking-hotpot.jpg?b=1&s=170667a&w=0&k=20&c=MUw8Ac5pmBh_ngwkKo50zJ6x5RBbk451F3BMJB8N4eE=",
          hashtags: ["cooking", "food", "shrimpscampi"],
          footer: "Professional chef and food blogger"
        },
        {
          id: 3,
          name: "Samantha Brown",
          username: "@sbrown",
          title: "Traveling the World",
          description: "Just arrived in Tokyo. So excited to explore this amazing city!",
          image: "https://media.istockphoto.com/id/1363398400/photo/woman-traveler-in-europa-alhambra-in-spain.jpg?b=1&s=170667a&w=0&k=20&c=5feqq_RYaiV2l8mk2ah8pIuDN4jk24jhsCi66DMxy-k=",
          hashtags: ["travel", "Tokyo"],
          footer: "Avid traveler and travel blogger"
        }
      ]
    }
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className='container'>
            {this.state.cards.map((record) => {
              return <Card
                description={record.description}
                hashtags={record.hashtags}
                image={record.image}
                key={record.id}
                name={record.name}
                title={record.title}
                username={record.username}
              />
            })}

          </div>
        </header>
      </div>
    )
  }
}

export default App
