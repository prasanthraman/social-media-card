import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faHeart, faComment, faShareSquare, faStar } from "@fortawesome/free-regular-svg-icons"
import { faHeart as Heart, faStar as Star } from "@fortawesome/free-solid-svg-icons"
import './options.css'

class Options extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            likes: 0,
            comments: Math.floor(Math.random() * (100 - 10 + 1) + 10),
            liked: false,
            starred: false
        }
    }

    handleLike = () => {
        this.setState({
            liked: !this.state.liked,
        })
    }

    handleStar = () => {
        this.setState({
            starred: !this.state.starred
        })
    }
    render() {

        return (
            <ul className="list">
                <li onClick={this.handleLike}>
                    <FontAwesomeIcon
                        className={this.state.liked ? "liked" : ""}
                        icon={this.state.liked ? Heart : faHeart}
                    />
                </li>
                <li>
                    <FontAwesomeIcon
                        icon={faComment}
                        className="comments"
                    />
                    {this.state.comments}
                </li>
                <li>
                    <FontAwesomeIcon
                        icon={faShareSquare}
                    />
                </li>
                <li onClick={this.handleStar} className="star">
                    <FontAwesomeIcon
                        className={this.state.starred ? "starred" : ""}
                        icon={this.state.starred ? Star : faStar}
                    />
                </li>
            </ul>
        )
    }
}

export default Options