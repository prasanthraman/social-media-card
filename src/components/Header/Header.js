import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUser } from "@fortawesome/free-solid-svg-icons"
import './Header.css'
class Header extends React.Component {

    render() {
        return (
            <div className="Header">
                <i>
                    <FontAwesomeIcon
                        icon={faUser}
                    />
                </i>
                <strong>{this.props.name}</strong>
                <p>{this.props.username}</p>
            </div>
        )
    }
}

export default Header