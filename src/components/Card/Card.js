import React from "react"
import Options from "../Options/Options"
import Header from "../Header/Header"
import "./Card.css"

class Card extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        let { name, username, title } = this.props
        return (<div className="card">
            <Header
                name={name}
                username={username}
                title={title}
            />
            <img src={this.props.image} alt={title} className="photo" />
            <div className="description">
                <strong>{username}</strong>
                <p>{this.props.description}</p>
            </div>
            <p className="tags">
                {this.props.hashtags.map((tag) => {
                    return `#${tag} `
                })}
            </p>

            <Options />
        </div>
        )
    }
}


export default Card